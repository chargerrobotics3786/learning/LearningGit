# Resolving An Issue

1. Create an Issue to add a new file.
2. Add that file in a new branch.
3. Create a Merge Request to merge your new branch to the main branch.
4. Accept the Merge Request

---------

## ***Hints***

<!-- markdownlint-disable MD033 -->
<details>
    <summary>Hint 1</summary>
    <blockquote>
You will need to go to GitLab to create an issue.
    </blockquote>
</details>

<details>
    <summary>Hint 2</summary>
    <blockquote>
Don't forget to push your branch before trying to create a Merge Request.
   </blockquote>
</details>

<details>
    <summary>Hint 3</summary>
    <blockquote>
Create the Merge Request from GitLab with a source branch of your new branch
and a target branch of the main branch.
   </blockquote>
</details>
<!-- markdownlint-enable MD033 -->