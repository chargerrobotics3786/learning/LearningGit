# Working With Remotes

1. Create a new branch on your local repository.
2. Make a commit on that branch.
3. Push the new branch up to GitLab.
4. Make another commit on that branch.
5. Push the branch again.
6. Have someone push another commit to your branch or edit a file in GitLab to
add a commit to your branch.
7. Pull down the changes to your branch.

---------

## ***Hints***

<!-- markdownlint-disable MD033 -->
<details>
    <summary>Hint 1</summary>
    <blockquote>
    <details>
        <summary>Command Line</summary>

Revisit [this document](../CommandLine/4-Branches.md) if you are having trouble
creating a branch.
    </details>
    <details>
        <summary>VSCode</summary>

Revisit [this document](../VSCode/4-Branches.md) if you are having trouble
creating a branch.
    </details>
    </blockquote>
</details>

<details>
    <summary>Hint 2</summary>
    <blockquote>
    <details>
        <summary>Command Line</summary>

Revisit [this document](../CommandLine/3-GitBasics.md) if you are having trouble
creating a commit.
    </details>
    <details>
        <summary>VSCode</summary>

Revisit [this document](../VSCode/3-GitBasics.md) if you are having trouble
creating a commit.
    </details>
   </blockquote>
</details>

<details>
    <summary>Hint 3</summary>
    <blockquote>
    <details>
        <summary>Command Line</summary>

Remember that you can use `git push` to push your currently checked out branch to
the remote.
    </details>
    <details>
        <summary>VSCode</summary>

Remember that you can use the `Push` button in the `More Actions...` menu to push
your checked out branch to the remote.

![Push Button](../resources/VSCode/push.png)
    </details>
    </blockquote>
</details>

<details>
    <summary>Hint 4</summary>
    <blockquote>
    <details>
        <summary>Command Line</summary>

Refer to [this document](../CommandLine/6-Remotes.md) if you need a refresher
on how to pull from a remote.
    </details>
    <details>
        <summary>VSCode</summary>

Refer to [this document](../VSCode/6-Remotes.md) if you need a refresher
on how to pull from a remote.
    </details>
    </blockquote>
</details>
<!-- markdownlint-enable MD033 -->
