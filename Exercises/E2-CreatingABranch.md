# Creating A Branch

1. Create a new branch named `first-branch`
2. Switch over to your new branch

---------

## ***Hints***

<!-- markdownlint-disable MD033 -->
<details>
    <summary>Hint 1</summary>
    <blockquote>
    <details>
        <summary>Command Line</summary>

Branches are created using `git branch [branch name]`
    </details>
    <details>
        <summary>VSCode</summary>

You can create a branch using the `Create Branch...` button in the
`More Actions...` menu:

![Create Branch](../resources/VSCode/create_branch.png)
    </details>
   </blockquote>
</details>

<details>
    <summary>Hint 2</summary>
    <blockquote>
    <details>
        <summary>Command Line</summary>

Branches can be checked out with `git checkout [branch name]`

**Extra Hint:** Branches can be created and checked out at once with
`git checkout -b [branch name]`
    </details>
    <details>
        <summary>VSCode</summary>

You can switch branches using the `Checkout to...` button:

![Checkout Branch](../resources/VSCode/checkout.png)
    </details>
    </blockquote>
</details>
<!-- markdownlint-enable MD033 -->
